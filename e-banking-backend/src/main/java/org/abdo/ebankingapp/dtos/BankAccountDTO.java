package org.abdo.ebankingapp.dtos;

import lombok.Data;

@Data
public class BankAccountDTO {
    private String type;
}
