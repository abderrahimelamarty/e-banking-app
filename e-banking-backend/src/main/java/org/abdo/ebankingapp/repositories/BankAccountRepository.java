package org.abdo.ebankingapp.repositories;

import org.abdo.ebankingapp.entities.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankAccountRepository extends JpaRepository<BankAccount,String> {
}
