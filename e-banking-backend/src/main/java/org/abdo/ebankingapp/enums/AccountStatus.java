package org.abdo.ebankingapp.enums;

public enum AccountStatus {
    CREATED, ACTIVATED, SUSPENDED
}
