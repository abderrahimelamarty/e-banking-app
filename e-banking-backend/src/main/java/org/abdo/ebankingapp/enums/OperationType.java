package org.abdo.ebankingapp.enums;

public enum OperationType {
    DEBIT, CREDIT
}
