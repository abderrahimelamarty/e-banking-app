package org.abdo.ebankingapp.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.abdo.ebankingapp.entities.BankAccount;
import org.abdo.ebankingapp.entities.CurrentAccount;
import org.abdo.ebankingapp.entities.SavingAccount;
import org.abdo.ebankingapp.enums.AccountStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class bankAccountRepositoryTest {
    @Autowired
    private BankAccountRepository bankAccountRepository;

    private BankAccount abdoAccount;
    private BankAccount elamartyAccount;

    @BeforeEach
    void init() {
        abdoAccount = new SavingAccount();
        abdoAccount.setCustomer(null);
        abdoAccount.setBalance(5000);
        abdoAccount.setCreatedAt(new Date());
        abdoAccount.setStatus(AccountStatus.CREATED);
        abdoAccount.setId(UUID.randomUUID().toString());

        elamartyAccount = new CurrentAccount();
        elamartyAccount.setCustomer(null);
        elamartyAccount.setBalance(5000);
        elamartyAccount.setCreatedAt(new Date());
        elamartyAccount.setStatus(AccountStatus.CREATED);
        elamartyAccount.setId(UUID.randomUUID().toString());

    }


    @Test
    @DisplayName("It should save the Account to the bank database")
    void save() {
        BankAccount account = bankAccountRepository.save( abdoAccount);
        assertNotNull(account);
        assertThat(account.getId()).isNotEqualTo(null);
    }



    @Test
    @DisplayName("It should return the Bank Account list with size of 2")
    void getAll() {
        bankAccountRepository.save(elamartyAccount);
        bankAccountRepository.save( abdoAccount);

        List<BankAccount> list = bankAccountRepository.findAll();

        assertNotNull(list);
        assertThat(list).isNotNull();
        assertEquals(2, list.size());
    }

    @Test
    @DisplayName("It should return the bank account by its id")
    void getById() {
        bankAccountRepository.save(abdoAccount);

        BankAccount account = bankAccountRepository.findById(abdoAccount.getId()).get();

        assertNotNull(account);
        assertEquals(5000.0, account.getBalance());
        assertThat(account.getCreatedAt()).isBefore(new Date(123,11,22));
    }

    @Test
    @DisplayName("It should update the BankAccount Status")
    void update() {

        bankAccountRepository.save(abdoAccount);

        BankAccount account = bankAccountRepository.findById(abdoAccount.getId()).get();
        account.setStatus(AccountStatus.ACTIVATED);
        BankAccount updatedaccount = bankAccountRepository.save(account);

        assertEquals(AccountStatus.ACTIVATED, updatedaccount.getStatus());
    }

    @Test
    @DisplayName("It should delete the BankAccount")
    void deleteMovie() {

        bankAccountRepository.save(abdoAccount);
        String id = abdoAccount.getId();

        bankAccountRepository.save(elamartyAccount);
        bankAccountRepository.delete(abdoAccount);

        List<BankAccount> list = bankAccountRepository.findAll();

        Optional<BankAccount> exitingAccount = bankAccountRepository.findById(id);

        assertEquals(1, list.size());
        assertThat(exitingAccount).isEmpty();

    }




}